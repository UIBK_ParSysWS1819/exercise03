cmake_minimum_required(VERSION 3.12)
project(exercise03)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "-Wall")

add_executable(01_mmul_c_static 01_mmul_c_static.c)
set_target_properties(01_mmul_c_static PROPERTIES COMPILE_FLAGS "-O3")

add_executable(02_mmul_c_dynamic 02_mmul_c_dynamic.c)
set_target_properties(02_mmul_c_dynamic PROPERTIES COMPILE_FLAGS "-O3")

add_executable(03_mmul_cpp_dynamic 03_mmul_cpp_dynamic.cpp)
set_target_properties(03_mmul_cpp_dynamic PROPERTIES COMPILE_FLAGS "-O3")
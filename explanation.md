## Results of vectorization width and loop unrolling

### 01_mmul_c_static.c

Link: https://godbolt.org/z/qMr-4M

|        |     gcc 8.2     |    clang 7.0    |      icc 19     |      MSVC 19    |
|--------|-----------------|-----------------|-----------------|-----------------|
| int    | 1, no unrolling | 8, unrolling: 4 | 8, no unrolling | 8, unrolling: 2 |
| float  | 1, no unrolling | 8, unrolling: 4 | 8, no unrolling | 8, unrolling: 2 |
| double | 1, no unrolling | 4, unrolling: 4 | 4, no unrolling | 4, unrolling: 2 |

### 02_mmul_c_dynamic.c

Link: https://godbolt.org/z/nojDK_

|        |     gcc 8.2     |    clang 7.0    |      icc 19     |      MSVC 19    |
|--------|-----------------|-----------------|-----------------|-----------------|
| int    | 1, no unrolling | 8, unrolling: 4 | 8, no unrolling | 1, no unrolling |
| float  | 1, no unrolling | 8, unrolling: 4 | 8, no unrolling | 1, unrolling: 4 |
| double | 1, no unrolling | 4, unrolling: 4 | 4, no unrolling | 1, unrolling: 4 |

### 03_mmul_cpp_dynamic.cpp

Link: https://godbolt.org/z/nJdVjY

|        |     gcc 8.2     |    clang 7.0    |      icc 19     |      MSVC 19    |
|--------|-----------------|-----------------|-----------------|-----------------|
| int    | 1, no unrolling | 1, unrolling: 2 | 8, no unrolling | 1, no unrolling |
| float  | 1, no unrolling | 1, unrolling: 2 | 8, no unrolling | 1, unrolling: 4 |
| double | 1, no unrolling | 1, unrolling: 2 | 4, no unrolling | 1, unrolling: 4 |

Vectorization width of 1 means 1 value is computed in one instruction. So it's basically a normal
multiplication or addition as usual without doing "parellel" operations. In assembler, this is mostly
done with instructions *mov*, *imul*, *add* in case of int, *vmovss*, *vmulss*, *vaddss* in case of float
or *vmovsd*, *vmulsd*, *vaddsd* in case of double.


Note, that in the third example code clang does no vectorization anymore, because the inner loops of the
multiplication are switched (Example 1 & 2: `i -> k -> j`, Example 3: `i -> j -> k`)


## Help for reading assembler codes

- XMM registers: 128-bit registers
- YMM registers: 256-bit registers

- *DWORD*: 32 bits / 4 bytes, e.g. used when loading int's from memory
- *QWORD*: 64 bits / 8 bytes, e.g. used when loading double's from memory
- *YMMWORD*: represents 256 bits, e.g. when loading from memory; depending on the data type of the data, more or less values have space in there, e.g. 8 values for int (32 bits each), 4 values for double (64 bits each): `256 (size of ymm register in bits) / 32 (size of float/int in bits) = 8` or `256 (size of ymm register in bits) / 64 (size of double in bits) = 4`

##### gcc 8.2:
double:
- *vmovsd*: Moves a **scalar double-precision** floating-point value from the source operand (second operand) to the destination operand (first operand).
- *vmulsd*: Multiplies the **low double-precision** floating-point value in the second source operand by the low double-precision floating-point value in the first source operand, and stores the double-precision floating-point result in the destination operand.
- *vaddsd*: Adds the **low double-precision** floating-point values from the second source operand and the first source operand and stores the double-precision floating-point result in the destination operand.

float:
- *vmovss*: Moves a **scalar single-precision** floating-point value from the source operand (second operand) to the destination operand (first operand).
- *vmulss*: Multiplies the **low single-precision** floating-point value from the second source operand by the low single-precision floating-point value in the first source operand, and stores the single-precision floating-point result in the destination operand.
- *vaddss*: Adds the **low single-precision** floating-point values from the second source operand and the first source operand, and stores the double-precision floating-point result in the destination operand.

int:
- *mov*: Copies the second operand (source operand) to the first operand (destination operand).
- *imul*: Performs a signed multiplication of two operands.
- *add*: Adds the destination operand (first operand) and the source operand (second operand) and then stores the result in the destination operand.

##### clang 7.0:
double:
- *vmulpd*: Multiply **packed double-precision** floating-point values from the first source operand with corresponding values in the second source operand, and stores the packed double-precision floating-point results in the destination operand.
- *vaddpd*: Add **two, four or eight packed double-precision** floating-point values from the first source operand to the second source operand, and stores the packed double-precision floating-point results in the destination operand.
- *vmovupd*: Moves **256 / 512 bits** (depending on encoded version) **of packed double-precision** floating-point values from the source operand (second operand) to the destination operand (first operand).
- *vmulsd*, *vaddsd*, *vmovsd*: see gcc above

float:
- *vmulps*: Multiply the **packed single-precision** floating-point values from the first source operand with the corresponding values in the second source operand, and stores the packed double-precision floating-point results in the destination operand.
- *vaddps*: Add **four, eight or sixteen packed single-precision** floating-point values from the first source operand with the second source operand, and stores the packed single-precision floating-point results in the destination operand.
- *vmovups*: Moves **256 / 512 bits** (depending on encoded version) **of packed single-precision** floating-point values from the source operand (second operand) to the destination operand (first operand).

int:
- *vpmulld*: Performs a **SIMD signed multiply of the packed signed dword/qword integers** from each element of the first source operand with the corresponding element in the second source operand.
- *vpaddd*: Performs a **SIMD add of the packed integers** from the source operand (second operand) and the destination operand (first operand), and stores the packed integer results in the destination operand.
- *vmovdqu*: Moves **256 bits of packed integer values** from the source operand (second operand) to the destination operand (first operand).

##### icc 19:
double:
- *vmovupd*, *vmovsd*: see above
- *vfmadd213pd*: Performs a **set of SIMD multiply-add computation on packed double-precision** floating-point values using three source operands and writes the multiply-add results in the destination operand. The destination operand is also the first source operand.
- *vfmadd213sd*: Performs a **SIMD multiply-add computation on the low double-precision** floating-point values using three source operands and writes the multiply-add result in the destination operand. The destination operand is also the first source operand.

float:
- *vmovups*: see above
- *vfmadd213ps*: Performs a **set of SIMD multiply-add computation on packed single-precision** floating-point values using three source operands and writes the multiply-add results in the destination operand. The destination operand is also the first source operand.
- *vmovss*: Moves a **scalar single-precision** floating-point value from the source operand (second operand) to the destination operand (first operand).
- *vfmadd213ss*: Performs a **SIMD multiply-add computation on single-precision** floating-point values using three source operands and writes the multiply-add results in the destination operand. The destination operand is also the first source operand.

int:
- *vpmulld*, *vpaddd*, *vmovdqu*: see above
- *mov*, *imul*, *add*: see above

##### MSVC 19:
double:
- *vmulpd*, *vaddpd*, *vmovupd*: see above
- *vmulsd*, *vaddsd*, *vmovsd*: see above

float:
- *vmulps*, *vaddps*, *vmovups*: see above

int:
- *vpmulld*, *vpaddd*, *vmovdqu*: see above

So icc already has one instruction, which does the multiply-add operation, whereas the other compilers separate instructions for multiply and add as we can see when looking at the assembly code.


Sources:
 - [x86 registers](https://en.wikipedia.org/wiki/X86#/media/File:Table_of_x86_Registers_svg.svg)
 - [Vectorization Tutorial](https://dendibakh.github.io/blog/2017/10/24/Vectorization_part1) (inkl. further links)
 - [Advanced Vector Extensions (AVX)](https://en.wikipedia.org/wiki/Advanced_Vector_Extensions)
 - [Word](https://en.wikipedia.org/wiki/Word_(computer_architecture))
 - Dokumentation provided within godbolt when right-clicking instructions


## Some additional compiler options for vectorization

### Gcc
To enable vectorization and output info in gcc 8.2 `-ftree-vectorize -fopt-info-vec-all`  could be used [(source)](https://stackoverflow.com/questions/33758993/why-doesnt-gcc-show-vectorization-information)
	
	outer offset from base address: 0
	outer constant offset from base address: 0
	outer step: 8000
	outer base alignment: 32
	outer base misalignment: 0
	outer offset alignment: 256
	outer step alignment: 64

This will give for example a `vector(4) double`.


### Clang

`-Rpass=loop-vectorize identifies loops that were successfully vectorized.` [Auto-Vectorization in LLVM](https://llvm.org/docs/Vectorizers.html)

### ICC
From User and Reference Guide for the Intel C++ Compiler 15.0  in the section [vec-report, Qvec-report ](https://software.intel.com/en-us/node/522949)

- Depricated option: `-vec-report`
- New option: `-qopt-report -qopt-report-phase=vec`

### MSVC

[/Qvec-report (Auto-Vectorizer Reporting Level)](https://msdn.microsoft.com/en-us/library/jj614596.aspx)

    /Qvec-report:1
    Outputs an informational message for loops that are vectorized.
    
    /Qvec-report:2
    Outputs an informational message for loops that are vectorized and for loops that are not vectorized, together with a reason code.
    
For reason code see [Vectorizer and Parallelizer Messages](https://msdn.microsoft.com/en-us/library/jj658585.aspx)
